import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		int T, a, b, c, i, res = 0;

	    Scanner in = new Scanner(System.in);
	    T = in.nextInt();

	    if(T<20){
	    	for(i=1; i<=T; ++i){
	    		 a = in.nextInt();
		         b = in.nextInt();
		         c = in.nextInt();

		         if(a>b && a>c){
		        	 if(b>c)
		        		 res = b;
		        	 else
		        		 res = c;
		         }
		         else if(b>a && b>c){
		        	 if(a>c)
		        		 res = a;
		        	 else
		        		 res = c;
		         }
		         else if(c>a && c>b){
		        	 if(a>b)
		        		res = a;
		        	 else
		        		 res = b;
		         }
		         System.out.println("Case " + i + ": " + res);
	    	}
	    }

	}

}
